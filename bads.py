import os
import glob
import pandas as pd
import csv
import threading
import time
import json
import requests
import urllib3
import hashlib
import random
import socket
from datetime import datetime
from urllib.parse import urlparse
from colorama import init
init()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

allResponseData = []
console_log = [None] * 16
status_thread = [False] * 16

clear = lambda: os.system('cls')
#######################################################################
def print_ba():
    print()
    print("| |  |||  |    |||  |||  | |  |||")
    print("| |  |    |    |    | |  |||  |")
    print("| |  |||  |    |    | |  |||  |||")
    print("|||  |    |    |    | |  | |  |")
    print("|||  |||  |||  |||  |||  | |  |||")
    print()
    print("|||  |||  |||       |||  ||   |||  | |")
    print("| |   |   | |       | |  | |  | |  |||")
    print("|||   |   ||        |||  | |  |||  |||")
    print("| |   |   | |       | |  | |  | |  | |")
    print("|||  |||  | |       | |  ||   | |  | |")
    print()
######################################################################
class ResponsData():
    
    def __init__(self):
        self.domain = [[],[]]                         #
        self.protokol = [[],[]]                       #   
        self.response_code = [[],[]]                  #
        self.ippAdd = [[],[]]                         #
        self.ssl_validation = ""                 #
        self.content = [[],[]]                        #
        self.identical_content = [[],[]]              #
        self.headers = [[],[]]                        #
    
    def print_data(self):
        print("domain:"+str(self.domain[0]))
        print("status_http:"+str(self.response_code[0]))
        print("status_https:"+str(self.response_code[1]))
        print("ssl:"+str(self.ssl_validation))
        print("content_equal:"+str(self.identical_content))
        
########################################################
def main():
    clear()
    print_ba()
    print("[1] Show result")
    print("[2] Scan")
    st = int(input("Select script type: "))
    
    if st == 1:
        show_result()
    elif st == 2:
        scan()
######################################################
def scan():
    mylist = [f for f in glob.glob("scan/*.csv")]
    for i in range(0,len(mylist)):
        print("["+str(i)+"]"+mylist[i])
        
    st = int(input("Select: "))
    with open(mylist[st], newline='') as f:
        reader = csv.reader(f)
        data = list(reader)
    #print(data[0][0])
    
    beginPos = [None] * 16
    beginPos[0] = 0
    for i in range(1,16):
        beginPos[i] = beginPos[i-1] + len(data) // 16
    
    endPos = [None] * 16
    endPos[0] = len(data) // 16
    for i in range(1,15):
        endPos[i] = endPos[i-1] + len(data) // 16
    endPos[15] = len(data)
    
    random.Random(4).shuffle(data)
    
    data_parts = [data[beginPos[0]:endPos[0]],
                 data[beginPos[1]:endPos[1]],
                 data[beginPos[2]:endPos[2]],
                 data[beginPos[3]:endPos[3]],
                 data[beginPos[4]:endPos[4]],
                 data[beginPos[5]:endPos[5]],
                 data[beginPos[6]:endPos[6]],
                 data[beginPos[7]:endPos[7]],
                 data[beginPos[8]:endPos[8]],
                 data[beginPos[9]:endPos[9]],
                 data[beginPos[10]:endPos[10]],
                 data[beginPos[11]:endPos[11]],
                 data[beginPos[12]:endPos[12]],
                 data[beginPos[13]:endPos[13]],
                 data[beginPos[14]:endPos[14]],
                 data[beginPos[15]:endPos[15]]]
    
    thread_1 = threading.Thread(target=thread_request, args=(0,data_parts[0]))
    thread_2 = threading.Thread(target=thread_request, args=(1,data_parts[1]))
    thread_3 = threading.Thread(target=thread_request, args=(2,data_parts[2]))
    thread_4 = threading.Thread(target=thread_request, args=(3,data_parts[3]))
    thread_5 = threading.Thread(target=thread_request, args=(4,data_parts[4]))
    thread_6 = threading.Thread(target=thread_request, args=(5,data_parts[5]))
    thread_7 = threading.Thread(target=thread_request, args=(6,data_parts[6]))
    thread_8 = threading.Thread(target=thread_request, args=(7,data_parts[7]))
    thread_9 = threading.Thread(target=thread_request, args=(8,data_parts[8]))
    thread_10 = threading.Thread(target=thread_request, args=(9,data_parts[9]))
    thread_11 = threading.Thread(target=thread_request, args=(10,data_parts[10]))
    thread_12 = threading.Thread(target=thread_request, args=(11,data_parts[11]))
    thread_13 = threading.Thread(target=thread_request, args=(12,data_parts[12]))
    thread_14 = threading.Thread(target=thread_request, args=(13,data_parts[13]))
    thread_15 = threading.Thread(target=thread_request, args=(14,data_parts[14]))
    thread_16 = threading.Thread(target=thread_request, args=(15,data_parts[15]))
    
    thread_1.start()
    thread_2.start()
    thread_3.start()
    thread_4.start()
    thread_5.start()
    thread_6.start()
    thread_7.start()
    thread_8.start()
    thread_9.start()
    thread_10.start()
    thread_11.start()
    thread_12.start()
    thread_13.start()
    thread_14.start()
    thread_15.start()
    thread_16.start()
    
    print("preparing scripts...")
    time.sleep(10)
    thread_console = threading.Thread(target=thread_log, args=())
    thread_console.start()
    
    thread_1.join()
    thread_2.join()
    thread_3.join()
    thread_4.join()
    thread_5.join()
    thread_6.join()
    thread_7.join()
    thread_8.join()
    thread_9.join()
    thread_10.join()
    thread_11.join()
    thread_12.join()
    thread_13.join()
    thread_14.join()
    thread_15.join()
    thread_16.join()
    
    thread_console.join()
    
    print("---------------------------------------------------")
    print("Scan is end.")
    print("Filter is active...")
    data_filter()
    
    
   # for i in allResponseData:
       # i.print_data()
      #  print("---------------------------------")
        
        
        
    now = datetime.now()
    dt_string = now.strftime("%d%m%Y%H%M%S")
    
    file_name = "result/"+dt_string+".csv"
    
    f = open(file_name,'w')
    f.write("domain,status_http,status_https,ssl,identical_content_http,identical_content_https\n")
    for data in allResponseData:
        f.write(str(data.domain[0]) + "," + str(data.response_code[0]) + "," + str(data.response_code[1]) + "," + str(data.ssl_validation) + "," + str(data.identical_content[0]).replace(","," ") + "," + str(data.identical_content[1]).replace(","," ") + "\n") 
    f.close()
    
    print("Result:"+'\033[32m'+file_name+'\033[39m')

#####################################################################3
def equal_content(content1,content2):
    result = False
    equal_per = 0
    
    len_c1 = len(content1)
    len_c2 = len(content2)
    
    if len_c1 == len_c2:
        if len_c1 > 100:
            for i in range(0,100):
                indexB = random.randint(0, len_c1-100)
                indexE = indexB + 100
                result_equal = content1.find(content2[indexB:indexE])
                if result_equal == -1:
                    pass
                else:
                    equal_per = equal_per + 1 
        else:
            for i in range(0, len_c1):
                if content1[i] == content2[i]:
                    equal_per = equal_per + 1
            equal_per = int((equal_per/len_c1) * 100)
    else:
        if abs(len_c1-len_c2) >= 700:
            return False
        else:
            if len_c1 <= 100 and len_c2 <= 100:
                pass
            elif len_c1 <= 100 and len_c2 >= 100:
                pass
            elif len_c1 >= 100 and len_c2 <= 100:
                pass
            elif len_c1 >= 100 and len_c2 >= 100:
                if len_c1 >= len_c2:
                    for i in range(0,100):
                        indexB = random.randint(0,len_c2-100)
                        indexE = indexB + 100
                        result_equal = content1.find(content2[indexB:indexE])
                        if result_equal == -1:
                            pass
                        else:
                            equal_per = equal_per + 1
                elif len_c1 <= len_c2:
                    for i in range(0,100):
                        indexB = random.randint(0,len_c1-100)
                        indexE = indexB + 100
                        result_equal = content2.find(content1[indexB:indexE])
                        if result_equal == -1:
                            pass
                        else:
                            equal_per = equal_per + 1
    
    if equal_per >= 85:
        return True
    else:
        return False
    return result
################################################################
def data_filter():
    for i in range(0,len(allResponseData)-1):
        for j in range(i+1,len(allResponseData)):
            try:
                #http and https
                if (allResponseData[i].response_code[0] == "200" or  allResponseData[i].response_code[0].find("redirect") != -1 ) and (allResponseData[i].response_code[1] == "200" or allResponseData[i].response_code[1].find("redirect") != -1):
                    if allResponseData[i].ippAdd[0] == allResponseData[j].ippAdd[0]:
                        if allResponseData[i].headers[0]["server"] == allResponseData[j].headers[0]["server"]:
                            isEqual = equal_content(allResponseData[i].content[0],allResponseData[j].content[0])
                            if isEqual == True:
                                allResponseData[i].identical_content[0].append(allResponseData[j].domain[0])
                                allResponseData[j].identical_content[0].append(allResponseData[i].domain[0])
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                    if allResponseData[i].ippAdd[0] == allResponseData[j].ippAdd[1]:
                        if allResponseData[i].headers[0]["server"] == allResponseData[j].headers[1]["server"]:
                            isEqual = equal_content(allResponseData[i].content[0],allResponseData[j].content[1])
                            if isEqual == True:
                                allResponseData[i].identical_content[0].append(allResponseData[j].domain[0])
                                allResponseData[j].identical_content[1].append(allResponseData[i].domain[0])
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                    if allResponseData[i].ippAdd[1] == allResponseData[j].ippAdd[0]:
                        if allResponseData[i].headers[1]["server"] == allResponseData[j].headers[0]["server"]:
                            isEqual = equal_content(allResponseData[i].content[1],allResponseData[j].content[0])
                            if isEqual == True:
                                allResponseData[i].identical_content[1].append(allResponseData[j].domain[0])
                                allResponseData[j].identical_content[0].append(allResponseData[i].domain[0])
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                    if allResponseData[i].ippAdd[1] == allResponseData[j].ippAdd[1]:
                        if allResponseData[i].headers[1]["server"] == allResponseData[j].headers[1]["server"]:
                            isEqual = equal_content(allResponseData[i].content[1],allResponseData[j].content[1])
                            if isEqual == True:
                                allResponseData[i].identical_content[1].append(allResponseData[j].domain[0])
                                allResponseData[j].identical_content[1].append(allResponseData[i].domain[0])
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
            except:
                pass
        
######################################################
def thread_log():
    j = 0
    while j == 0:
        clear()
        for line in console_log:            
            print(line)
            i = 0
            for act in status_thread:
                if act == False:
                    i = i + 1
            if i == 16:
                j = 1
        time.sleep(2)
###########################################################
def thread_request(n,ls):
    lock = threading.Lock()
    lock.acquire() 
    status_thread[n] = True
    lock.release()
    
    for i in range(0,len(ls)):
        dom = ls[i][0]
        
        #print(dom,n)
        
        resData = ResponsData()
        resData.domain = [dom,dom]                      #domain set
        resData.protokol = ["http","https"]             #protokol set
       
        lock.acquire() 
        console_log[n] = '\033[32m'+str(i+1) + "/" + str(len(ls)) + "\t" + '\033[39m' + dom
        lock.release()
        
        http_status_code = -1
        https_status_code = -1
        
        http_content = ""
        https_content = ""
        
        ssl_cert = ""
        
        http_ip = ""
        https_ip = ""
        
        http_headers = []
        https_headers = []
        
        
    
        #-------------------http-----------------------------------------#
        try:
            request_http = requests.get("http://"+dom,allow_redirects=False,timeout=5,verify=False)
                
            #print(request_http.headers["server"])
            #print("IP:",socket.gethostbyname(dom))
            #print(request_http.url)
            #print(request_http.status_code)
            #print(request_http.ip)
            #print(request_http.content)
            
            if request_http.status_code < 300 or request_http.status_code >= 400:
                http_status_code = str(request_http.status_code)
                http_content = request_http.content
                
                
                #aaa = str(http_content).split("\n")
                #print(len(http_content))
                
                try:
                    http_ip = str(socket.gethostbyname(dom))
                except:
                    pass
                http_headers = request_http.headers
            else:
                request_http = requests.get("http://"+dom,allow_redirects=True,timeout=5,verify=False)
                if request_http.url.find(dom) != -1:
                    http_status_code = str(request_http.status_code)
                else:
                    http_status_code = "redirect-" + request_http.url
                http_content = request_http.content
                try:
                    http_ip = str(socket.gethostbyname(dom))
                except:
                    pass
                http_headers = request_http.headers
                
        except requests.exceptions.RequestException as e:
            http_status_code = "Error"
        #---------------------https--------------------------------------------#
        try:
            request_https = requests.get("https://"+dom,allow_redirects=False,timeout=5,verify=False)
            
            #print(request_https.url)
            #print(request_https.status_code)
            #print(request_https.ip)
            #print(request_https.content)
            if request_https.status_code < 300 or request_https.status_code >= 400:
                https_status_code = str(request_https.status_code)
                try:
                    https_ip = str(socket.gethostbyname(dom))
                except:
                    pass
                https_content = request_https.content
                https_headers = request_https.headers
            else:
                request_https = requests.get("https://"+dom,allow_redirects=True,timeout=5,verify=False)
                if request_https.url.find(dom) != -1:
                    https_status_code = str(request_https.status_code)
                else:
                    https_status_code = "redirect-" + request_https.url
                try:
                    https_ip = str(socket.gethostbyname(dom))
                except:
                    pass
                https_content = request_https.content
                https_headers = request_https.headers
                
        except requests.exceptions.RequestException as e:
            http_status_code = "Error"
        #-------------------------------ssl-------------------------------------#
        try:
            requests.get("https://"+dom, verify=True)
            ssl_cert = "Var"
        except requests.exceptions.SSLError as e:
            ssl_cert = "Yoxdur"
        except:
            pass
    
        #-------------------------------------------------------------------------------#
        resData.ippAdd = [http_ip,https_ip]                                        #ip addr set
        resData.ssl_validation = ssl_cert                                          #ssl val set
        resData.content = [http_content,https_content]                             #content set
        resData.response_code = [http_status_code,https_status_code]                 #status code set
        resData.headers = [http_headers,https_headers]                             #headers set
        #s = json.dumps(resData.__dict__)
        #print(s)
        #print(resData)
        lock.acquire() 
        allResponseData.append(resData)
        lock.release()
        
        
    #print(responseDataList[0].domain)
    lock.acquire() 
    console_log[n] = str(i+1) + "/" + str(len(ls)) + "\t"+'\033[31m'+"FINISH" + '\033[39m'
    status_thread[n] = False
    lock.release()
    
    
##############################################################
def show_result():
    mylist = [f for f in glob.glob("result/*.csv")]
    for i in range(0,len(mylist)):
        print("["+str(i)+"]"+mylist[i])
        
    st = int(input("Select: "))
    pd.set_option('display.max_rows', None)
    data = pd.read_csv(mylist[st])
    print(data)
    
#########################################################
if __name__ == "__main__":
    main()
########################################################
